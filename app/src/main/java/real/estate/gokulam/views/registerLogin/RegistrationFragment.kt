package real.estate.gokulam.views.registerLogin

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import customs.AsteriskPasswordTransformationMethod
import customs.CustomRadioButton
import kotlinx.android.synthetic.main.fragment_registration.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import real.estate.gokulam.R
import real.estate.gokulam.model.register.ModelRegister
import real.estate.gokulam.model.register.mediatorNames.Mediator
import real.estate.gokulam.model.register.mediatorNames.ModelMediatorName
import real.estate.gokulam.utils.*
import real.estate.gokulam.utils.MessageUtils.log
import real.estate.gokulam.utils.Utils.*
import real.estate.gokulam.views.MainActivity
import real.estate.gokulam.views.colletionAgent.CollectionAgent
import real.estate.gokulam.views.dashboard.Dashboard
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class RegistrationFragment : Fragment() {
    var snackbar: Snackbar? = null
    var dialog: Dialog? = null
    var checkMediator: String = ""
    var customeType: String = ""
    var mediatorName: String = ""
    var imageURi: Uri? = null
    private var mCurrentPhotoPath: String? = ""
    var mediatorListValues: List<Mediator> = ArrayList<Mediator>()

    var sessionManager: SessionManager? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as MainActivity).setNavigationBarDisable(getString(R.string.registration))
        if (MainActivity.action_settings != null) {
            MainActivity.action_settings.isVisible = false
        }
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sessionManager = SessionManager(activity)

        confirm_password.transformationMethod = AsteriskPasswordTransformationMethod()
        password_rgs.transformationMethod = AsteriskPasswordTransformationMethod()

        /*  confirm_password.typeface = MessageUtils.setType(activity,Fonts.MEDIUM)
          password_rgs.typeface = MessageUtils.setType(activity,Fonts.MEDIUM)*/


        submit.setOnClickListener {
            if (Utils.haveNetworkConnection(activity)) {

                val id = rg.checkedRadioButtonId
                if (id != -1) {
                    val userType = rg.findViewById<CustomRadioButton>(id).text.toString()
                    if (userType.equals(activity!!.getString(R.string.customer))) {  // select customer
                        if (!checkMediator.isEmpty()) {
                            if (checkMediator.equals(activity!!.getString(R.string.mediator))) {   // if select mediator check below mediator Name also
                                //mediatorName = mediator_name_sp.selectedItem.toString()

                                if (!mediatorName.isEmpty()) {
                                    validationAll(userType, checkMediator, mediatorName)
                                } else {
                                    snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Select Mediator Name")
                                }
                            } else {
                                validationAll(userType, checkMediator, "")
                            }
                        } else {
                            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Select Lead Source")
                        }
                    } else {
                        validationAll(userType, "", "")  // if select Mediator
                    }
                } else {
                    snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Select User Type")
                }
            } else {
                snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, getString(R.string.check_inter_net))

            }
        }



        mediator_name_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                try {
                    if (mediatorListValues.size != 0) {
//                        Toast.makeText(activity, " " + mediatorListValues[position].id, Toast.LENGTH_SHORT).show()
                        mediatorName = mediatorListValues[position].id.toString()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        rg.setOnCheckedChangeListener { _, checkedId ->
            val radioButton = rg.findViewById<CustomRadioButton>(checkedId)
            customeType = radioButton.text.toString()
            if (customeType.equals(activity!!.getString(R.string.customer))) {
                customer_lay.visibility = View.VISIBLE
            } else {
                customer_lay.visibility = View.GONE
            }
        }


        rg_customer.setOnCheckedChangeListener { group, checkedId ->
            val radioButton = rg_customer.findViewById<CustomRadioButton>(checkedId)
            checkMediator = radioButton.text.toString()
            if (checkMediator.equals(activity!!.getString(R.string.mediator))) {
                mediator_lay.visibility = View.VISIBLE
                //log("call_mediator_names")

                callMediatorNames()

            } else {
                mediator_lay.visibility = View.GONE
            }
        }

        reg_img_rl?.setOnClickListener {
            try {
                showDetailsFprpermission();
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


    }

    private fun showDetailsFprpermission() {
        try {
            val confirmatioandialog = Dialog(activity)
            confirmatioandialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            confirmatioandialog.setContentView(R.layout.dialog_confirmation_permission)
            confirmatioandialog.window!!.setBackgroundDrawableResource(R.drawable.rounded_rect_border)

            if (
                    ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            ) {
                confirmatioandialog.show()
                val title: TextView
                val reason1: TextView
                val reason2: TextView
                val confirmation: TextView
                title = confirmatioandialog.findViewById(R.id.permission_title)
                reason1 = confirmatioandialog.findViewById(R.id.reason_of_permis1)
                reason2 = confirmatioandialog.findViewById(R.id.reason_of_permis2)
                confirmation = confirmatioandialog.findViewById(R.id.confirmation_granted)
                title.text = getString(R.string.permissionfor_storage)
                reason1.text = getString(R.string.reason_for_storage)
                reason2.text = getString(R.string.reason_for_storag)
                confirmation.setOnClickListener {
                    if (dialog != null && dialog!!.isShowing) {
                        dialog!!.dismiss()
                    }
                    if (confirmatioandialog != null && confirmatioandialog.isShowing) {
                        confirmatioandialog.dismiss()
                    }
                    requestpermision()
                }

            } else {
                requestpermision()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun requestpermision() {
        try {

            if (
                    ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
            ) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) &&
                        ActivityCompat.shouldShowRequestPermissionRationale(activity!!, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                        ActivityCompat.shouldShowRequestPermissionRationale(activity!!, android.Manifest.permission.CAMERA)
                ) {
                    requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA), 234)
                } else {
                    requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA), 234)
                }

            } else {
                showSetProfileOptionsDialog()
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun callMediatorNames() {
        dialog = MessageUtils.showDialog(activity)
        //http://192.168.2.9:8000/api/real/mediatorlist
        val callModel = Utils.getInstance(activity).onCallMediatorName()
        callModel.enqueue(object : Callback<ModelMediatorName> {
            override fun onFailure(call: Call<ModelMediatorName>?, t: Throwable?) {
                Log.d("namess", "asasas fail")
                MessageUtils.dissmiss(dialog)
                try {
                    val msg = MessageUtils.setFailurerMessage(activity, t!!.message)
                    snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, msg)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

            override fun onResponse(call: Call<ModelMediatorName>?, response: Response<ModelMediatorName>?) {
                MessageUtils.dissmiss(dialog)
                try {
                    if (response != null) {
                        if (response.isSuccessful) {
                            val mediatorNamesModel: ModelMediatorName = response.body()
                            if (mediatorNamesModel.success) {
                                Log.d("namess", "asasas")
                                Log.d("namess", "asasas" + mediatorNamesModel)
                                var count = 0
                                mediatorListValues = mediatorNamesModel.data
                                if (mediatorListValues != null) {
                                    count = mediatorListValues.size
                                }

                                if (count != 0) {
                                    val strings: MutableList<String> = ArrayList()
                                    for (items in mediatorListValues) {
                                        strings.add(items.name)
                                    }
                                    Log.d("stringssdsd", "" + strings.size)
                                    mediator_name_sp.adapter = ArrayAdapter<String>(activity, R.layout.simple_item, R.id.item_name, strings)
                                } else {
                                    MessageUtils.showSnackBar(activity, snack_view_rgs, "Mediator Not Found")
                                }
                            } else {
                                MessageUtils.showSnackBar(activity, snack_view_rgs, mediatorNamesModel.message)
                            }
                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showToastMessage(activity, msg)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        )
    }

    private fun validationAll(userType: String, checkMediator: String, mediatorName: String) {
        val name = name.text.toString()
        val email = email.text.toString()
        val mobileNumber = mobileNumber.text.toString()
        val address = address.text.toString()
        val password = password_rgs.text.toString()
        val confirmPassword = confirm_password.text.toString()
        val aadharCard = aadhar_card_no.text.toString()
        val panCard = pan_card.text.toString()
        val accountNumber = account_number.text.toString()
        val ifscCode = ifsc_code.text.toString()
        val branch = branch.text.toString()
        val bankName = bank_name.text.toString()

        if (

                validationMantory(userType, name, email, mobileNumber, address, password, confirmPassword,imageURi)

        ) {
            val hashMap = HashMap<String, String>()
            hashMap["name"] = name
            hashMap["email"] = email
            hashMap["mobile_number"] = mobileNumber
            hashMap["address"] = address
            hashMap["password"] = password
            hashMap["confirm_password"] = confirmPassword
            hashMap["aadhar_card"] = aadharCard
            hashMap["pan_card"] = panCard
            hashMap["account_number"] = accountNumber
            hashMap["ifsc_code"] = ifscCode
            hashMap["branch"] = branch
            hashMap["bank_name"] = bankName
            hashMap["user_type"] = userType
            hashMap["check_mediator"] = checkMediator
            hashMap["mediator_name"] = mediatorName
            hashMap["token_reference"] = "token_reference"
            onCallBackRegister(hashMap)
        }
    }

    private fun validationMantory(userType: String, name: String, email: String, mobileNumber: String, address: String, password: String, confirmPassword: String, imageURi: Uri?): Boolean {
        log(userType)
        if (name.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Name can't be empty")
            return false
        } else if (email.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Email Id can't be empty")
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Enter valid Email ID")
            return false
        } else if (mobileNumber.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Mobile Number can't be empty")
            return false
        } else if (mobileNumber.length != 10) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Enter Valid Mobile Number ")
            return false
        } else if (address.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Address can't be empty")
            return false
        } else if (password.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Password can't be empty")
            return false
        } else if (confirmPassword.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Confirm Password can't be empty")
            return false
        } else if (confirmPassword != password) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Password not Matched")
            return false
        }else if(imageURi==null ||imageURi.toString().isEmpty()){
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Please Select profile Image")
            return false
        }

        return true
    }

    private fun onCallBackRegister(hashMap: HashMap<String, String>) {
        dialog = MessageUtils.showDialog(activity)
        val image = File(PathUtil.getPath(activity,imageURi))
        val requset = RequestBody.create(MediaType.parse("multipart/form-data"),image)
        val imagePart = MultipartBody.Part.createFormData("image", image.name, requset)
        val onRegister = Utils.getInstance(activity).onCallREgisterwithImage("register",hashMap,imagePart)
        onRegister.enqueue(object : Callback<ModelRegister> {
            override fun onFailure(call: Call<ModelRegister>?, t: Throwable?) {
                MessageUtils.dissmiss(dialog)
                try {
                    val msg = MessageUtils.setFailurerMessage(activity, t!!.message)
                    snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, msg)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

            override fun onResponse(call: Call<ModelRegister>?, response: Response<ModelRegister>?) {
                MessageUtils.dissmiss(dialog)
                try {
                    if (response!!.isSuccessful) {
                        val modelRegister = response.body()
                        if (modelRegister.success) {
                            if (modelRegister.data != null) {
                                sessionManager?.createLoginSession(modelRegister.data.userdetail.name, modelRegister.data.userdetail.email, modelRegister.data.userdetail.mobile_no, modelRegister.data.userdetail.id.toString(), modelRegister.data.token, modelRegister.data.userdetail.user_type.toString())
                                MainActivity.mAppEmailID.text = modelRegister.data.userdetail.email
                                MainActivity.mAppUserName.text = modelRegister.data.userdetail.name
                                MessageUtils.showToastMessage(activity, modelRegister.message)
                                if (SessionManager.getUserType(activity!!).toLowerCase() == USER_CUSTOMER.toLowerCase()) {
                                    FragmentCallUtils.passFragmentWithoutAnim(activity!!, Dashboard())
                                } else if (SessionManager.getUserType(activity!!).toLowerCase() == USER_MEDIATOR.toLowerCase()) {
                                    FragmentCallUtils.passFragmentWithoutAnim(activity!!, Dashboard())
                                } else if (SessionManager.getUserType(activity!!).toLowerCase() == USER_COLLECTION_AGENT.toLowerCase()) {
                                    FragmentCallUtils.passFragmentWithoutAnim(activity!!, CollectionAgent())
                                }

                            } else {
                                snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "User Details not found")
                            }
                        } else {
                            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, modelRegister.message)
                        }
                    } else {
                        val msg = MessageUtils.setErrorMessage(response.code())
                        snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, msg)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        })
    }

    private fun validation(userType: String, name: String, email: String, mobileNumber: String, address: String, password: String, confim_password: String, pan_card: String, aadhar_card: String, account_number: String, ifsc_code: String, branch: String, bank_name: String): Boolean {
        log(userType)
        if (name.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Name can't be empty")
            return false
        } else if (email.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Email Id can't be empty")
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Enter valid Email ID")
            return false
        } else if (mobileNumber.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Mobile Number can't be empty")
            return false
        } else if (mobileNumber.length != 10) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Enter Valid Mobile Number ")
            return false
        } else if (address.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Address can't be empty")
            return false
        } else if (password.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Password can't be empty")
            return false
        } else if (confim_password.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Confirm Password can't be empty")
            return false
        } else if (confim_password != password) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Password not Matched")
            return false
        } else if (aadhar_card.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Aadhar Card Number can't be empty")
            return false
        } else if (pan_card.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "PAN Card Number can't be empty")
            return false
        } else if (bank_name.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Bank Name can't be empty")
            return false
        } else if (account_number.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Account Number can't be empty")
            return false
        } else if (branch.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "Branch Name can't be empty")
            return false
        } else if (ifsc_code.isEmpty()) {
            snackbar = MessageUtils.showSnackBar(activity, snack_view_rgs, "IFSC Code can't be empty")
            return false
        }
        return true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(snackbar)
        MessageUtils.dissmiss(dialog)
    }

    private fun showSetProfileOptionsDialog() {
        val options = arrayOf(getString(R.string.uploadphot), getString(R.string.takeaphoto))

        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(getString(R.string.profile_image))
                .setItems(options) { dialog, which ->
                    if (which == 0) {
                        requestMedia()
                    } else if (which == 1) {

                        requsetCamera()
                    }
                }
        builder.create().show()
    }

    private fun requsetCamera() {
        try {
            val values = ContentValues(1)
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
            val fileUri = activity?.contentResolver
                    ?.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            values)
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(activity?.packageManager) != null) {
                mCurrentPhotoPath = fileUri.toString()
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                        or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                startActivityForResult(intent, 7777)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    private fun requestMedia() {
        try {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_PICK
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), 8888)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode.equals(7777) && resultCode == Activity.RESULT_OK && data != null) {
            val cursor = activity?.contentResolver?.query(Uri.parse(mCurrentPhotoPath),
                    Array(1) { android.provider.MediaStore.Images.ImageColumns.DATA },
                    null, null, null)
            cursor?.moveToFirst()
            val photoPath = cursor?.getString(0)
            cursor?.close()
            val file = File(photoPath)
            imageURi = Uri.fromFile(file)
            Log.d("Log", "yes  pic Selected  " + imageURi.toString())
            ImageUtils.displayRoundImageFromUrl(activity, PathUtil.getPath(activity, imageURi), reg_image_view_profile)
        } else if (requestCode.equals(8888) && resultCode == Activity.RESULT_OK && data != null) {
            imageURi = data.data!!
            ImageUtils.displayRoundImageFromUrl(activity, PathUtil.getPath(activity, imageURi), reg_image_view_profile)
            Log.d("Log", "yes  pic Selected  " + data.toString())
        } else {
            Log.d("Log", "No pic Selected")
        }

    }

    private fun getTempFileUri(doNotUseFileProvider: Boolean): Uri? {
        var uri: Uri? = null
        try {
            val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            val tempFile = File.createTempFile("Profile" + System.currentTimeMillis(), ".jpg", path)
/*
            if (Build.VERSION.SDK_INT >= 24 && !doNotUseFileProvider) {
                uri = FileProvider.getUriForFile(activity!!, "com.sendbird.android.sample.provider", tempFile)
            } else {
                uri = Uri.fromFile(tempFile)
            }*/
            uri = Uri.fromFile(tempFile)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return uri
    }
}